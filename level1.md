# 1. Horizontal and Vertical Scaling 

 No organization can plan with accuracy in the early days. The resources needed and expected might change over time.  
 
  Imagine, you have more requests than ever, so what do you do? , yeah we have to **add resources**.  
  There are 2 ways to add resources.
 *  Horizontal Scaling
 *  Vertical Scaling

## 1. Horizontal Scaling  
 Horizontal Scaling refers to adding more systems or machines. This is known as *scaling out*.

 Here we might have to split up the logic as we have to do parallelly, since we have many computers.
[Reference link](https://www.section.io/blog/scaling-horizontally-vs-vertically/)

## 2. Vertical Scaling
Vertical Scaling refers to adding more power to the existing  resources. That is adding more RAM,processors ets.

Here we dont have to  split the logics as we are computing it in the same machine. 

# 2.Using Messaging queues

 When we receive many requests we can handle upto some point. But if it's way over the capacity, we will be loosing the requests as we are already serving other requests. 

 To serve all requests we have to carefuly store all requests. And serve these requests on **first come first serve** basis. We have to use messaging queue. This is a temporary storage, which stores the request until it is taken from the queue. Yes, the queue is meant to store un-attended request. Once request is read from queue there is no need for it to be in the queue. It improves web application loading time. 
[Reference link](https://stackify.com/message-queues-12-reasons/)

# 3.Full Text Search

 Imagine in your database there are millions of entries, and user is searching for word "**cat**". So we have to display all the post which contains "cat".    
 
 Yes, this can be done by MYSQL. But it will be very slow. MYSQL is famous for safely storing data, not speed.  
 So what do we do now? we have to use something called as *Elastic Search*. This is famous for speed, but not so famous to store data. 

 So we combine both technology so we can have both speed and non-volatile.  
[Reference link](https://mentormate.medium.com/elasticsearch-how-to-add-full-text-search-to-your-database-ee2f3ea4d3f3)


# 4.Different types of caching, and Database caching

 Caching is the best way to increase speed of a application.
 
  Types of cache are:
 * Cache-Aside
 * Read-Through
 * Write-Through
 * Write-Around 
 * Write-Back

 Database caching improves scalability by distributing query workload from backend to multiple cheap front-end systems. It allows flexibility in the processing of data, for example, the data of Platinum customers can be cached while that of ordinary customers are not. Caching can improve availability of data, by providing continued service for applications that depend only on cached tables even if the backend server is unavailable. 
[Reference link](https://codeahoy.com/2017/08/11/caching-strategies-and-how-to-choose-the-right-one/)